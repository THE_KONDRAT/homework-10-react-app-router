import { UserInternal } from "../UserInternal"

export const addUser = (user: UserInternal) => {
    return {
        type: '[USER_STORAGE_STATE] USER_ADD',
        payload: user
    }
};