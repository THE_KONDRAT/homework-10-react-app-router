import { User } from "../User";

export default function UserReducer (state: User | null = null, action: any) {
    switch (action.type){
        case '[USER_STATE] USER_SET':
            return state = action.payload;
        default:
            return state;
    }
}