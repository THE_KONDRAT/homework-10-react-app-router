import { User } from "../User"


export const setUser = (user: User) => {
    return {
        type: '[USER_STATE] USER_SET',
        payload: user
    }
};


export const clearUser = () => {
    return {
        type: '[USER_STATE] USER_SET',
        payload: null
    }
};