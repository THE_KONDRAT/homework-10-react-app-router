import { configureStore } from "@reduxjs/toolkit"
import UserReducer from "./UserReducer"
import UsersStorageReducer from "./UsersStorageReducer"

const store = configureStore(
    {
        reducer: {
            user: UserReducer,
            userStorage: UsersStorageReducer
        },
    }
);

export default store;