import { UserInternal } from "../UserInternal";

export default function UserStorageReducer(state: Array<UserInternal> = Array<UserInternal>(), action: any){
    switch (action.type){
        case '[USER_STORAGE_STATE] USER_ADD':
            return state.concat([action.payload]);
        default:
            return state;
    }
}