import './App.css';
import {
  Route,
  Routes,
  BrowserRouter
} from "react-router-dom";

import HomePage from './pages/HomePage';
import RegisterPage from './pages/RegisterPage';
import LoginPage from './pages/LoginPage';
import NotFoundPage from './pages/NotFoundPage';
import LoginLayout from './pages/LoginLayout';

function App() {
  return (
    <BrowserRouter>
      <Routes>
          <Route index element={<HomePage />} />
          <Route path="register" element={<LoginLayout title='REGISTER' ><RegisterPage /></LoginLayout>} />
          <Route path="login" element={<LoginLayout title='LOGIN'><LoginPage /></LoginLayout>} />
          <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
