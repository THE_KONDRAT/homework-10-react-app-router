import { useDispatch, useSelector } from "react-redux"
import { NavigateFunction, useNavigate } from "react-router-dom"
import { addUser } from "../stateManagement/UserStorageActions"
import { UserInternal, CheckHashPassword as CheckPassword, GetPasswordHash } from "../UserInternal"
import { Dispatch } from "react";
import { AnyAction } from "@reduxjs/toolkit";

export default function LoginComponent(){
    let firstName = '';
    let lastName = '';
    let login = '';
    let password = '';
    const userList = useSelector((x: any) => x.userStorage) as UserInternal[] | null;
    const dispatch = useDispatch();
    const navigate = useNavigate();
    return(
        <div className="border border-5 border-start-0 border-end-0 border-light rounded-4">
            <div className="container my-3 ">
                <div className="row mb-2">
                    <div className="col">
                        <input type='text' className='form-control-lg' placeholder='Enter first name' name='firstName' onChange={(e:any) => {firstName = e.target.value}}/>
                    </div>
                </div>
                <div className="row mb-4">
                    <div className="col">
                        <input type='text' className='form-control-lg' placeholder='Enter last name' name='lastName' onChange={(e:any) => {lastName = e.target.value}}/>
                    </div>
                </div>
                <div className="row mb-2">
                    <div className="col">
                        <input type='text' className='form-control-lg' placeholder='Enter login' name='loginInput' onChange={(e:any) => {login = e.target.value}}/>
                    </div>
                </div>
                <div className="row mb-2">
                    <div className="col">
                        <input type='password' className='form-control-lg' placeholder='Enter password' name='passwordInput' onChange={(e:any) => {password = e.target.value}}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <button className="btn btn-primary fs-5" onClick={() => Register(firstName, lastName, login, password, userList, dispatch, navigate)}><b>Зарегистрироваться</b></button>
                    </div>
                </div>
            </div>
        </div>
    );
}

function Register(firstName: string, lastName: string, login: string, password: string, userList: UserInternal[] | null, dispatch: Dispatch<AnyAction>, navigate: NavigateFunction){
    let user = userList?.find(q => q.login === login && CheckPassword(password, q.hashPassword))
    if (!user){
        const newUser: UserInternal = {firstName:firstName, lastName:lastName, login:login, hashPassword:GetPasswordHash(password)};
        dispatch(addUser(newUser));
        navigate("/login");
    }
};