import logo from '../logo.svg';
import { useSelector } from "react-redux"
import { Link } from "react-router-dom";

export interface IGreetingsComponentProps{
    name: string;
}

export default function GreetingsComponent(){
    const backgroundBootstrapClasses = "bg-secondary bg-gradient p-2 border border-5 border-start-0 border-end-0 border-light rounded-4";
    const navLinkBootstrapClasses="nav-link bg-dark rounded-pill p-3 py-0 bg-opacity-50 border border-1 border-primary";
    const greetText = 'Приветствую тебя';
    const user = useSelector((x: any) => x.user);
    return(
        <div className="App">
            <header className="App-header">
                { <img src={logo} className={user != undefined ? "App-logo" : "App-logo-sm"} alt="logo" /> }
                <div>
                    <div>
                        <div className={backgroundBootstrapClasses}>
                            {user === null &&
                                <span className="text-light fs-3">
                                    <b>{greetText}, некто</b>
                                </span>
                            }
                            {user !== null &&
                                <span className="text-warning fs-3">
                                    <b>{greetText}, {user.firstName}</b>
                                </span>
                            }
                        </div>
                    </div>
                    <br />
                </div>
                <nav>
                    <ul className='nav flex-column'>
                        <li className='nav-item'>
                            <Link className={navLinkBootstrapClasses} to={'/login'}>Страница логина</Link>
                        </li>
                    </ul>
                </nav>
            </header>
        </div>
    );
}