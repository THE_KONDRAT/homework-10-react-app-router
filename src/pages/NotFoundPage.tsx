import logo from "../logo.svg"
export default function NotFoundComponent(){
    const backgroundStyle={
        borderStyle: "solid",
        borderColor: "Gray",
        borderWidth: "0.4rem 0 0.4rem 0",
        borderRadius: "0.8rem",
        padding: "0 0.75rem 0.45rem 0.75rem",
        fontWeight: "600",
        fontSize: "4rem",
        color: "Orange"
    };
    return(
        <div className="App">
            <header className="App-header">
                <div style={backgroundStyle}>
                    <span>404</span>
                </div>
            <img src={logo} className="App-logo-sm" alt="logo" />
            </header>
        </div>
    );
}