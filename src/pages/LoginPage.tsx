import { useDispatch, useSelector } from "react-redux"
import { Dispatch } from "react";
import { AnyAction } from "@reduxjs/toolkit";
import { NavigateFunction, useNavigate } from "react-router-dom"
import { setUser, clearUser } from "../stateManagement/UserActions"
import { UserInternal, CheckHashPassword as CheckPassword } from "../UserInternal"
import { User } from "../User"

export default function LoginComponent(){
    const backgroundBootstrapClasses = "border border-5 border-start-0 border-end-0 border-light rounded-4";
    let login = '';
    let password = '';
    const currentUser = useSelector((x: any) => x.user) as User | null;
    let userList = useSelector((x: any) => x.userStorage) as UserInternal[] | null;
    const dispatch = useDispatch();
    const navigate = useNavigate();
    return(
        <div className={backgroundBootstrapClasses}>
            <div className="container my-3 ">

                <div className="row mb-2">
                    <div className="col">
                        <input type='text' className='form-control-lg' placeholder='Enter login' name='loginInput' onChange={(e:any) => {login = e.target.value}}/>
                    </div>
                </div>
                <div className="row mb-2">
                    <div className="col">
                        <input type='password' className='form-control-lg' placeholder='Enter password' name='passwordInput' onChange={(e:any) => {password = e.target.value}}/>
                    </div>
                </div>
                <div className="row justify-content-between">
                    <div className="col-auto">
                        <button className="btn btn-primary fs-5" onClick={() => Login(login, password, currentUser, userList, dispatch, navigate)}><b>Войти</b></button>
                    </div>
                    <div className="col-auto">
                        {currentUser !== null && <button className="btn btn-danger fs-5" onClick={() => LogOut(currentUser, dispatch, navigate)}>Выйти</button>}
                        {currentUser === null && <button className="btn btn-info fs-5" onClick={() => navigate("/register")}><b>Регистрация</b></button>}
                    </div>
                </div>

            </div>
        </div>
    );
}

export function Login(login: string, password: string, currentUser: User | null, userList: UserInternal[] | null, dispatch: Dispatch<AnyAction>, navigate: NavigateFunction){
    if (!currentUser){
        let user = userList?.find(q => q.login === login && CheckPassword(password, q.hashPassword))
        if (user){
            const newUser: User = {firstName:user.firstName, lastName:user.lastName, login:user.login};
            dispatch(setUser(newUser));
            navigate("/");
        }
    }
};

export function LogOut(currentUser: User | null, dispatch: Dispatch<AnyAction>, navigate: NavigateFunction){
    if (currentUser){
        dispatch(clearUser());
        navigate("/");
    }
};