import { ReactNode } from "react";
interface MyProps {
    title: string
    children?: ReactNode;
 }

export default function LoginLayout(props: MyProps){
    const backgroundBootstrapClasses = "bg-secondary bg-gradient p-0 py-2 border border-5 border-start-0 border-end-0 border-info rounded-pill";
    return(
        <div className="App">
            <header className="App-header">
                {
                    /*
                    <img src={logo} className="App-logo-sm" alt="logo" />
                    */
                }
                <div>
                    <div className={backgroundBootstrapClasses}>
                        <span className="text-info fs-">
                            <b>{props.title}</b>
                        </span>
                    </div>
                    <br />
                    <div>
                        {props.children}
                    </div>
                </div>
            </header>
        </div>
    );
}