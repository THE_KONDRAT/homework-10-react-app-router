export type UserInternal = {firstName: string, lastName: string, login: string, hashPassword: string};

export function CheckHashPassword(password: string, hashPassword: string){
    return GetPasswordHash(password) === hashPassword; //пока без хеширования
}

export function GetPasswordHash(password: string){
    return password; //пока без хеширования
}